/*ESTE JAVA ES UTILIZADO PARA QUE CUANDO ESCRIBAMOS CUALQUIER VALOR SE QUEDE LA TRANSICION POR ESO LEE QUE CUANDO TENGAMOS AUNQUE SEA 1 DATO 
SE QUEDE FIJO CASO CONTRARIO SE EMUEVA*/
var input = document.getElementsByClassName('formulario__input');
for (var i =0; i< input.length; i++){
   input[i].addEventListener('keyup', function(){
      if(this.value.length >= 1){
         this.nextElementSibling.classList.add('fijar');
      }else {
         this.nextElementSibling.classList.remove('fijar');
      }
   })
}
