const bd =[
    {"Id":0,"Apellido":"Apellido: Blondet", "Nombre":"Nombre: Darel", "Semestre": "Semestre: Quinto", 
    "Paralelo":" Paralelo: A", "Direccion":"Direccion: Portoviejo", "Telefono": "Telefono: 0968581680",
    "Correo":"Correo: darel2214@gmail.com"},

    {"Id":1,"Apellido":"Apellido: Zambrano", "Nombre":"Nombre: David", "Semestre": "Semestre: Cuarto", 
    "Paralelo":" Paralelo: C", "Direccion":"Direccion: Montecristi", "Telefono": "Telefono: 0987654141",
    "Correo":"Correo: david007@gmail.com"},

    {"Id":2,"Apellido":"Apellido: Zea", "Nombre":"Nombre: Maria", "Semestre": "Semestre: Sexto", 
    "Paralelo":" Paralelo: A", "Direccion":"Direccion: Jipijapa", "Telefono": "Telefono: 0963258741",
    "Correo":"Correo: maria123@gmail.com"},

    {"Id":3,"Apellido":"Apellido: Espinoza", "Nombre":"Nombre: Andrea", "Semestre": "Semestre: Primero", 
    "Paralelo":" Paralelo: B", "Direccion":"Direccion: Manta", "Telefono": "Telefono: 0968587848",
    "Correo":"Correo: andr3a@gmail.com"},

    {"Id":4,"Apellido":"Apellido: Espinoza", "Nombre":"Nombre: Javier", "Semestre": "Semestre: Segundo", 
    "Paralelo":" Paralelo: A", "Direccion":"Direccion: Manta", "Telefono": "Telefono: 0965848532",
    "Correo":"Correo: 7avi07@gmail.com"},

    {"Id":5,"Apellido":"Apellido: Baque", "Nombre":"Nombre: Dayanna", "Semestre": "Semestre: Quinto", 
    "Paralelo":" Paralelo: A", "Direccion":"Direccion: Manta", "Telefono": "Telefono: 098421545",
    "Correo":"Correo: dayanna@gmail.com"},
    
    {"Id":6,"Apellido":"Apellido: Lopez", "Nombre":"Nombre: Jenn", "Semestre": "Semestre: Noveno", 
    "Paralelo":" Paralelo: D", "Direccion":"Direccion: Bariloche", "Telefono": "Telefono: 091245878",
    "Correo":"Correo: jennloxx@gmail.com"},

    {"Id":7,"Apellido":"Apellido: Lopez", "Nombre":"Nombre: Lolo", "Semestre": "Semestre: Decimo", 
    "Paralelo":" Paralelo: B", "Direccion":"Direccion: Manta", "Telefono": "Telefono: No tiene",
    "Correo":"Correo: lolomiuau25@gmail.com"},

    {"Id":8,"Apellido":"Apellido: Intriago", "Nombre":"Nombre: Carlota", "Semestre": "Semestre: Decimo", 
    "Paralelo":" Paralelo: A", "Direccion":"Direccion: Manta", "Telefono": "Telefono: No tiene",
    "Correo":"Correo: carlcat123@gmail.com"},
    
    {"Id":9,"Apellido":"Apellido: Blondet", "Nombre":"Nombre: Elias", "Semestre": "Semestre: Septimo", 
    "Paralelo":" Paralelo: D", "Direccion":"Direccion: Jipijapa", "Telefono": "Telefono: 0987485961",
    "Correo":"Correo: eli4asb@gmail.com"},
]

const estudiantes = document.querySelectorAll('.nom_estudiante');

estudiantes.forEach((estudiante)=>{
    estudiante.addEventListener('click', (nombre)=>{
        let id=nombre.target.getAttribute('estu-id');
        bd.forEach((estudiante)=>{
            if(id == estudiante.Id){
                const verDetalle=nombre.target.parentElement.lastElementChild;
                verDetalle.innerHTML=`
                                    <div class="lista"
                                        <div class="nom">
                                        <h2>Datos del estudiante:</h2>
                                        <h2 class="list-inline-item footer-menu"><a class="nav-link" style="color:#000000" href="http://127.0.0.1:5501/inicio.html"> REGRESAR</a></h2>
                                        <p>${estudiante.Nombre}</p>
                                        <p>${estudiante.Apellido}</p>
                                        <p>${estudiante.Correo}</p>
                                        <p>${estudiante.Telefono}</p>
                                        <p>${estudiante.Direccion}</p>
                                        <p>${estudiante.Semestre}</p>
                                        <p>${estudiante.Paralelo}</p>
                                        </div> 
                                    </div>`

            }
        })
    })
})

